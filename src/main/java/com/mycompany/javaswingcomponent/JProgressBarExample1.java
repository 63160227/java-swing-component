/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author a
 */
public class JProgressBarExample1 extends JFrame {

    JProgressBar jp;
    int i = 0, num = 0;

    JProgressBarExample1() {
        jp = new JProgressBar(0, 2000);
        jp.setBounds(40, 40, 160, 30);
        jp.setValue(0);
        jp.setStringPainted(true);
        add(jp);
        setSize(260, 160);
        setLayout(null);
    }

    public void iterate() {
        while (i <= 2000) {
            jp.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        JProgressBarExample1 m = new JProgressBarExample1();
        m.setVisible(true);
        m.iterate();
    }
}
