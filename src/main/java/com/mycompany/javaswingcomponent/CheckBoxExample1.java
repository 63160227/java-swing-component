/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JCheckBox;
import javax.swing.JFrame;

/**
 *
 * @author a
 */
public class CheckBoxExample1 {
    CheckBoxExample1(){  
        JFrame frame = new JFrame("CheckBox Example");  
        JCheckBox checkBox1 = new JCheckBox("C++");  
        checkBox1.setBounds(100, 100, 50, 50);  
        JCheckBox checkBox2 = new JCheckBox("Java", true);  
        checkBox2.setBounds(100, 160, 50, 50);  
        frame.add(checkBox1);  
        frame.add(checkBox2);  
        frame.setSize(300,400);  
        frame.setLayout(null);  
        frame.setVisible(true);  
     }  
public static void main(String args[])  
    {  
    new CheckBoxExample1();  
    }}  

