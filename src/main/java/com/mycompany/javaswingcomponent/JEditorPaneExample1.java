/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author a
 */
public class JEditorPaneExample1 {

    JFrame frame = null;

    public static void main(String[] a) {
        (new JEditorPaneExample1()).test();
    }

    private void test() {
        frame = new JFrame("JEditorPane Test");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 200);
        JEditorPane myPane = new JEditorPane();
        myPane.setContentType("text/plain");
        myPane.setText("Sleeping is necessary for a healthy body."
                + " But sleeping in unnecessary times may spoil our health, wealth and studies."
                + " Doctors advise that the sleeping at improper timings may lead for obesity during the students days.");
        frame.setContentPane(myPane);
        frame.setVisible(true);
    }
}
