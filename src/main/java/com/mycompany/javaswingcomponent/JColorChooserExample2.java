/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author a
 */
public class JColorChooserExample2 extends JFrame implements ActionListener {

    JFrame frame;
    JButton b;
    JTextArea ta;

    JColorChooserExample2() {
        frame = new JFrame("Color Chooser Example.");
        b = new JButton("Pad Color");
        b.setBounds(200, 230, 100, 30);
        ta = new JTextArea();
        ta.setBounds(10, 10, 270, 200);
        b.addActionListener(this);
        frame.add(b);
        frame.add(ta);
        frame.setLayout(null);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(this, "Choose", Color.CYAN);
        ta.setBackground(c);
    }

    public static void main(String[] args) {
        new JColorChooserExample2();
    }
}
