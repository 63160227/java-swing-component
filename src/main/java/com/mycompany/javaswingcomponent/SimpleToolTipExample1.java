/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 *
 * @author a
 */
public class SimpleToolTipExample1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        
        JPasswordField value = new JPasswordField();
        value.setBounds(100, 100, 100, 30);
        value.setToolTipText("Enter your Password");
        JLabel L1= new JLabel("Password:");
        L1.setBounds(20, 100, 80, 30);
        
        frame.add(value);
        frame.add(L1);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
