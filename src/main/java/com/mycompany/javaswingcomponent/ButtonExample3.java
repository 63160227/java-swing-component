/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author a
 */
public class ButtonExample3 {

    ButtonExample3() {
        JFrame f = new JFrame("Button Example");
        JButton b = new JButton(new ImageIcon("D:\\register-button.jpg"));
        b.setBounds(70, 70, 350, 80);
        f.add(b);
        f.setSize(500, 300);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new ButtonExample3();
    }
}
