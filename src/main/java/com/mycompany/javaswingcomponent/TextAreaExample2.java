/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 *
 * @author a
 */
public class TextAreaExample2 implements ActionListener {

    JLabel L1, L2;
    JTextArea area;
    JButton button;

    TextAreaExample2() {
        JFrame frame = new JFrame();
        L1 = new JLabel();
        L1.setBounds(50, 25, 100, 30);
        L2 = new JLabel();
        L2.setBounds(170, 25, 100, 30);
        area = new JTextArea();
        area.setBounds(20, 75, 250, 200);
        button = new JButton("Count Words");
        button.setBounds(100, 300, 120, 30);
        button.addActionListener(this);
        frame.add(L1);
        frame.add(L2);
        frame.add(area);
        frame.add(button);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String text = area.getText();
        String words[] = text.split("\\s");
        L1.setText("Words: " + words.length);
        L2.setText("Characters: " + text.length());
    }

    public static void main(String[] args) {
        new TextAreaExample2();
    }
}
