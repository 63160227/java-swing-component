/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author a
 */
public class JDialogExample1 {

    private static JDialog d;

    JDialogExample1() {
        JFrame frame = new JFrame();
        d = new JDialog(frame, "Dialog Example", true);
        d.setLayout(new FlowLayout());
        JButton button = new JButton("OK");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialogExample1.d.setVisible(false);
            }
        });
        d.add(new JLabel("Click button to continue."));
        d.add(button);
        d.setSize(300, 300);
        d.setVisible(true);
    }

    public static void main(String args[]) {
        new JDialogExample1();
    }
}
