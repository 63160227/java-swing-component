/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author a
 */
public class LabelExample1 {

    public static void main(String args[]) {
        JFrame frame = new JFrame("Label Example");
        JLabel L1, L2;
        L1 = new JLabel("The First");
        L1.setBounds(60, 60, 100, 30);
        L2 = new JLabel("The Second");
        L2.setBounds(60, 100, 100, 30);
        frame.add(L1);
        frame.add(L2);
        frame.setSize(300,300);  
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
