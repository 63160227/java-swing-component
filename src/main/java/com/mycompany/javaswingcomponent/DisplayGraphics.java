/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

/**
 *
 * @author a
 */
public class DisplayGraphics extends Canvas {

    @Override
    public void paint(Graphics g) {
        g.drawString("Hello", 40, 40);
        setBackground(Color.pink);
        g.fillRect(130, 30, 300, 80);
        g.drawOval(50, 90, 50, 60);
        setForeground(Color.cyan);
        g.fillOval(150, 220, 100, 150);
        g.drawArc(30, 200, 40, 50, 0, 180);
        g.fillArc(30, 130, 40, 50, 180, 40);

    }

    public static void main(String[] args) {
        DisplayGraphics main = new DisplayGraphics();
        JFrame frame = new JFrame();
        frame.add(main);
        frame.setLocationRelativeTo(null);
        frame.setSize(400, 400);
        frame.setVisible(true);
        frame.pack();
    }
}

