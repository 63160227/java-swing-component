/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author a
 */
public class TextFieldExample1 {

    public static void main(String args[]) {
        JFrame frame = new JFrame("TextField Example");
        JTextField t1, t2;
        t1 = new JTextField("Firstname ");
        t1.setBounds(50, 100, 200, 30);
        t2 = new JTextField("Surname");
        t2.setBounds(50, 170, 200, 30);
        frame.add(t1);
        frame.add(t2);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
