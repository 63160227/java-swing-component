/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author a
 */
public class JTableExample1 {

    JFrame frame;

    JTableExample1() {
        frame = new JFrame();
        String data[][] = {{"1", "JM", "00109"},
        {"2", "J", "00004"},
        {"3", "M", "00025"}};
        String column[] = {"NUMBER", "NAME", "ID"};
        JTable jt = new JTable(data, column);
        jt.setBounds(30, 40, 200, 300);
        JScrollPane sp = new JScrollPane(jt);
        frame.add(sp);
        frame.setSize(300, 400);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new JTableExample1();
    }
}

