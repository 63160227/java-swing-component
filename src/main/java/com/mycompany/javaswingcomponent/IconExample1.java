/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;

/**
 *
 * @author a
 */
public class IconExample1 {

    IconExample1() {
        Frame frame = new Frame();
        Image icon = Toolkit.getDefaultToolkit().getImage("D:\\correct_operation__609597.png");
        frame.setIconImage(icon);
        frame.setLayout(null);
        frame.setSize(300, 200);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new IconExample1();
    }
}
