/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author a
 */
public class ButtonExample1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Test Button");
        JButton b = new JButton("Click Here");
        b.setBounds(130, 90, 100, 30);
        frame.add(b);
        frame.setSize(380, 380);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }
}
