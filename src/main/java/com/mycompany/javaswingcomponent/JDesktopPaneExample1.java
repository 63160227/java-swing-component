/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

/**
 *
 * @author a
 */
public class JDesktopPaneExample1 extends JFrame {

    public JDesktopPaneExample1() {
        CustomDesktopPane desktopPane = new CustomDesktopPane();
        Container contentPane = getContentPane();
        contentPane.add(desktopPane, BorderLayout.CENTER);
        desktopPane.display(desktopPane);

        setTitle("JDesktopPane Example");
        setSize(400, 450);
        setVisible(true);
    }

    public static void main(String args[]) {
        new JDesktopPaneExample1();
    }
}

class CustomDesktopPane extends JDesktopPane {

    int numFrames = 3, x = 30, y = 30;

    public void display(CustomDesktopPane dp) {
        for (int i = 0; i < numFrames; ++i) {
            JInternalFrame frame = new JInternalFrame("Internal Frame " + i, true, true, true, true);

            frame.setBounds(x, y, 250, 85);
            Container c1 = frame.getContentPane();
            c1.add(new JLabel("I love my country"));
            dp.add(frame);
            frame.setVisible(true);
            y += 85;
        }
    }
}
