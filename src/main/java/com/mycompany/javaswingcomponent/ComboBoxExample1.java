/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JComboBox;
import javax.swing.JFrame;

/**
 *
 * @author a
 */
public class ComboBoxExample1 {

    JFrame frame;

    ComboBoxExample1() {
        frame = new JFrame("ComboBox Example");
        String country[] = {"Thai", "Korea", "U.S.A", "Japan", "Australia"};
        JComboBox cb = new JComboBox(country);
        cb.setBounds(50, 50, 90, 20);
        frame.add(cb);
        frame.setLayout(null);
        frame.setSize(400, 500);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new ComboBoxExample1();
    }
}
