/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author a
 */
public class JOptionPaneExample2 {

    JFrame frame;

    JOptionPaneExample2() {
        frame = new JFrame();
        String name = JOptionPane.showInputDialog(frame, "Enter Name");
    }

    public static void main(String[] args) {
        new JOptionPaneExample2();
    }
}
