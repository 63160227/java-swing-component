/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

/**
 *
 * @author a
 */
public class RadioButtonExample1 {

    JFrame frame;

    RadioButtonExample1() {
        frame = new JFrame();
        JRadioButton r1 = new JRadioButton("1. Male");
        JRadioButton r2 = new JRadioButton("2. Female");
        r1.setBounds(75, 50, 100, 30);
        r2.setBounds(75, 100, 100, 30);
        ButtonGroup bg = new ButtonGroup();
        bg.add(r1);
        bg.add(r2);
        frame.add(r1);
        frame.add(r2);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new RadioButtonExample1();
    }
}
