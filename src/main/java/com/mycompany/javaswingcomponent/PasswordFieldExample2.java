/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author a
 */
public class PasswordFieldExample2 {
    public static void main(String[] args) {    
    JFrame frame =new JFrame("Password Field Example");    
     final JLabel label1 = new JLabel();            
     label1.setBounds(20,150, 200,50);  
     final JPasswordField value = new JPasswordField();   
     value.setBounds(100,75,100,30);   
     JLabel L1=new JLabel("Username :");    
        L1.setBounds(20,20, 80,30);    
        JLabel l2=new JLabel("Password :");    
        l2.setBounds(20,75, 80,30);    
        JButton b = new JButton("Login");  
        b.setBounds(100,120, 80,30);    
        final JTextField text = new JTextField();  
        text.setBounds(100,20, 100,30);    
                frame.add(value); frame.add(L1); frame.add(label1); frame.add(l2); frame.add(b); frame.add(text);  
                frame.setSize(300,300);    
                frame.setLayout(null);    
                frame.setVisible(true);     
                b.addActionListener(new ActionListener() {  
                @Override
                public void actionPerformed(ActionEvent e) {       
                   String data = "Username " + text.getText();  
                   data += ", Password : "   
                   + new String(value.getPassword());   
                   label1.setText(data);          
                }  
             });   
}  
}  
