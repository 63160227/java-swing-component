/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author a
 */
public class LabelExample2 extends Frame implements ActionListener {

    JTextField tf;
    JLabel lebel;
    JButton button;

    LabelExample2() {
        tf = new JTextField();
        tf.setBounds(50, 100, 150, 20);
        lebel = new JLabel();
        lebel.setBounds(100, 100, 250, 20);
        button = new JButton("Find IP");
        button.setBounds(50, 150, 95, 30);
        button.addActionListener(this);
        add(button);
        add(tf);
        add(lebel);
        setSize(400, 400);
        setLayout(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            String host = tf.getText();
            String ip = java.net.InetAddress.getByName(host).getHostAddress();
            lebel.setText("IP of " + host + " is: " + ip);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public static void main(String[] args) {
        new LabelExample2();
    }
}
