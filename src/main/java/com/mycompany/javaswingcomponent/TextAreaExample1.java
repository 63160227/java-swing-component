/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author a
 */
public class TextAreaExample1 {

    TextAreaExample1() {
        JFrame frame = new JFrame();
        JTextArea area = new JTextArea("Type a message in this box.");
        area.setBounds(20, 30, 200, 200);
        frame.add(area);
        frame.setSize(280, 280);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new TextAreaExample1();
    }
}
