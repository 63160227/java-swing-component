/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author a
 */
public class JExecutable1 {

    JExecutable1() {
        JFrame frame = new JFrame();

        JButton b = new JButton("click");
        b.setBounds(130, 100, 100, 40);

        frame.add(b);

        frame.setSize(400, 200);
        frame.setLayout(null);
        frame.setVisible(true);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new JExecutable1();
    }
}
