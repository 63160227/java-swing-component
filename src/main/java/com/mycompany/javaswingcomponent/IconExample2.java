/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswingcomponent;

import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author a
 */
public class IconExample2 {

    IconExample2() {
        JFrame f = new JFrame();
        Image icon = Toolkit.getDefaultToolkit().getImage("‪‪D:\\6705921_preview.png");
        f.setIconImage(icon);
        f.setLayout(null);
        f.setSize(300, 200);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new IconExample2();
    }

}
